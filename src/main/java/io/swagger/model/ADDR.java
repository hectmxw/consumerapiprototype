package io.swagger.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * ADDR
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-06T02:32:31.422Z")

public class ADDR {
  @JsonProperty("l1")
  private String l1 = null;

  @JsonProperty("l2")
  private String l2 = null;

  @JsonProperty("zip")
  private Integer zip = null;

  public ADDR(String l1, String l2, Integer zip) {
    this.l1 = l1;
    this.l2 = l2;
    this.zip = zip;
  }

  public ADDR l1(String l1) {
    this.l1 = l1;
    return this;

  }



   /**
   * Get l1
   * @return l1
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getL1() {
    return l1;
  }

  public void setL1(String l1) {
    this.l1 = l1;
  }

  public ADDR l2(String l2) {
    this.l2 = l2;
    return this;
  }

   /**
   * Get l2
   * @return l2
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getL2() {
    return l2;
  }

  public void setL2(String l2) {
    this.l2 = l2;
  }

  public ADDR zip(Integer zip) {
    this.zip = zip;
    return this;
  }

   /**
   * Get zip
   * @return zip
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public Integer getZip() {
    return zip;
  }

  public void setZip(Integer zip) {
    this.zip = zip;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ADDR ADDR = (io.swagger.model.ADDR) o;
    return Objects.equals(this.l1, ADDR.l1) &&
        Objects.equals(this.l2, ADDR.l2) &&
        Objects.equals(this.zip, ADDR.zip);
  }

  @Override
  public int hashCode() {
    return Objects.hash(l1, l2, zip);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ADDR {\n");

    sb.append("    l1: ").append(toIndentedString(l1)).append("\n");
    sb.append("    l2: ").append(toIndentedString(l2)).append("\n");
    sb.append("    zip: ").append(toIndentedString(zip)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
/**
 * Party
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-28T05:57:17.005Z")

public class Party   {
  @JsonProperty("producer_id")
  private String producerId = null;

  @JsonProperty("phone_nr")
  private String phoneNr = null;

  @JsonProperty("first_name")
  private String firstName = null;

  @JsonProperty("last_name")
  private String lastName = null;

  @JsonProperty("source_sys")
  private String sourceSys = null;

  @JsonProperty("policy_nr")
  private String policyNr = null;

  @JsonProperty("role")
  private String role = null;

  @JsonProperty("ssn")
  private Integer ssn = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("dob")
  private String dob = null;

  @JsonProperty("street_addr_1")
  private String streetAddr1 = null;

  @JsonProperty("street_addr_2")
  private String streetAddr2 = null;

  @JsonProperty("city")
  private String city = null;

  @JsonProperty("state")
  private String state = null;

  @JsonProperty("zip")
  private Integer zip = null;

  @JsonProperty("epi")
  private Integer epi = null;

  @JsonProperty("party_key_id")
  private String partyKeyId = null;

  public Party producerId(String producerId) {
    this.producerId = producerId;
    return this;
  }

   /**
   * Get producerId
   * @return producerId
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getProducerId() {
    return producerId;
  }

  public void setProducerId(String producerId) {
    this.producerId = producerId;
  }

  public Party phoneNr(String phoneNr) {
    this.phoneNr = phoneNr;
    return this;
  }

   /**
   * Get phoneNr
   * @return phoneNr
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getPhoneNr() {
    return phoneNr;
  }

  public void setPhoneNr(String phoneNr) {
    this.phoneNr = phoneNr;
  }

  public Party firstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

   /**
   * Get firstName
   * @return firstName
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public Party lastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

   /**
   * Get lastName
   * @return lastName
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Party sourceSys(String sourceSys) {
    this.sourceSys = sourceSys;
    return this;
  }

   /**
   * Get sourceSys
   * @return sourceSys
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getSourceSys() {
    return sourceSys;
  }

  public void setSourceSys(String sourceSys) {
    this.sourceSys = sourceSys;
  }

  public Party policyNr(String policyNr) {
    this.policyNr = policyNr;
    return this;
  }

   /**
   * Get policyNr
   * @return policyNr
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getPolicyNr() {
    return policyNr;
  }

  public void setPolicyNr(String policyNr) {
    this.policyNr = policyNr;
  }

  public Party role(String role) {
    this.role = role;
    return this;
  }

   /**
   * Get role
   * @return role
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public Party ssn(Integer ssn) {
    this.ssn = ssn;
    return this;
  }

   /**
   * Get ssn
   * @return ssn
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public Integer getSsn() {
    return ssn;
  }

  public void setSsn(Integer ssn) {
    this.ssn = ssn;
  }

  public Party name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Party dob(String dob) {
    this.dob = dob;
    return this;
  }

   /**
   * Get dob
   * @return dob
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getDob() {
    return dob;
  }

  public void setDob(String dob) {
    this.dob = dob;
  }

  public Party streetAddr1(String streetAddr1) {
    this.streetAddr1 = streetAddr1;
    return this;
  }

   /**
   * Get streetAddr1
   * @return streetAddr1
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getStreetAddr1() {
    return streetAddr1;
  }

  public void setStreetAddr1(String streetAddr1) {
    this.streetAddr1 = streetAddr1;
  }

  public Party streetAddr2(String streetAddr2) {
    this.streetAddr2 = streetAddr2;
    return this;
  }

   /**
   * Get streetAddr2
   * @return streetAddr2
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getStreetAddr2() {
    return streetAddr2;
  }

  public void setStreetAddr2(String streetAddr2) {
    this.streetAddr2 = streetAddr2;
  }

  public Party city(String city) {
    this.city = city;
    return this;
  }

   /**
   * Get city
   * @return city
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public Party state(String state) {
    this.state = state;
    return this;
  }

   /**
   * Get state
   * @return state
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public Party zip(Integer zip) {
    this.zip = zip;
    return this;
  }

   /**
   * Get zip
   * @return zip
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public Integer getZip() {
    return zip;
  }

  public void setZip(Integer zip) {
    this.zip = zip;
  }

  public Party epi(Integer epi) {
    this.epi = epi;
    return this;
  }

   /**
   * Get epi
   * @return epi
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public Integer getEpi() {
    return epi;
  }

  public void setEpi(Integer epi) {
    this.epi = epi;
  }

  public Party partyKeyId(String partyKeyId) {
    this.partyKeyId = partyKeyId;
    return this;
  }

   /**
   * Get partyKeyId
   * @return partyKeyId
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getPartyKeyId() {
    return partyKeyId;
  }

  public void setPartyKeyId(String partyKeyId) {
    this.partyKeyId = partyKeyId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Party party = (Party) o;
    return Objects.equals(this.producerId, party.producerId) &&
        Objects.equals(this.phoneNr, party.phoneNr) &&
        Objects.equals(this.firstName, party.firstName) &&
        Objects.equals(this.lastName, party.lastName) &&
        Objects.equals(this.sourceSys, party.sourceSys) &&
        Objects.equals(this.policyNr, party.policyNr) &&
        Objects.equals(this.role, party.role) &&
        Objects.equals(this.ssn, party.ssn) &&
        Objects.equals(this.name, party.name) &&
        Objects.equals(this.dob, party.dob) &&
        Objects.equals(this.streetAddr1, party.streetAddr1) &&
        Objects.equals(this.streetAddr2, party.streetAddr2) &&
        Objects.equals(this.city, party.city) &&
        Objects.equals(this.state, party.state) &&
        Objects.equals(this.zip, party.zip) &&
        Objects.equals(this.epi, party.epi) &&
        Objects.equals(this.partyKeyId, party.partyKeyId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(producerId, phoneNr, firstName, lastName, sourceSys, policyNr, role, ssn, name, dob, streetAddr1, streetAddr2, city, state, zip, epi, partyKeyId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Party {\n");
    
    sb.append("    producerId: ").append(toIndentedString(producerId)).append("\n");
    sb.append("    phoneNr: ").append(toIndentedString(phoneNr)).append("\n");
    sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
    sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
    sb.append("    sourceSys: ").append(toIndentedString(sourceSys)).append("\n");
    sb.append("    policyNr: ").append(toIndentedString(policyNr)).append("\n");
    sb.append("    role: ").append(toIndentedString(role)).append("\n");
    sb.append("    ssn: ").append(toIndentedString(ssn)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    dob: ").append(toIndentedString(dob)).append("\n");
    sb.append("    streetAddr1: ").append(toIndentedString(streetAddr1)).append("\n");
    sb.append("    streetAddr2: ").append(toIndentedString(streetAddr2)).append("\n");
    sb.append("    city: ").append(toIndentedString(city)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    zip: ").append(toIndentedString(zip)).append("\n");
    sb.append("    epi: ").append(toIndentedString(epi)).append("\n");
    sb.append("    partyKeyId: ").append(toIndentedString(partyKeyId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


package io.swagger.model;

/**
 * Created by hectmxw on 5/30/2017.
 */
public class UserInput {

    private String firstName;

    private String lastName;

    private String policyNumber;

    public UserInput() {
    }

    public UserInput(String firstName, String lastName, String policyNumber, String phoneNumber, String producerID, String SSN) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.policyNumber = policyNumber;
        this.phoneNumber = phoneNumber;
        this.producerID = producerID;
        this.SSN = SSN;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProducerID() {
        return producerID;
    }

    public void setProducerID(String producerID) {
        this.producerID = producerID;
    }

    public String getSSN() {
        return SSN;
    }

    public void setSSN(String SSN) {
        this.SSN = SSN;
    }

    private String phoneNumber;

    private String producerID;

    private String SSN;



}

package io.swagger.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * Party object exposed by API
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-28T05:57:17.005Z")

public class SwaggerParty {

  @JsonProperty("first_name")
  private String firstName = null;

  @JsonProperty("last_name")
  private String lastName = null;

  @JsonProperty("ssn")
  private Integer ssn = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("policy_nr")
  private String policyNr = null;

  @JsonProperty("address")
  private ADDR address = null;

  public SwaggerParty(String firstName, String lastName, Integer ssn, String name, String policyNr, String l1, String l2, Integer zip) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.ssn = ssn;
    this.name = name;
    this.policyNr = policyNr;
    this.address = new ADDR(l1, l2, zip);
  }

  public SwaggerParty policyNr(String policyNr) {
    this.policyNr = policyNr;
    return this;
  }


  public SwaggerParty firstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

   /**
   * Get firstName
   * @return firstName
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public SwaggerParty lastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

   /**
   * Get lastName
   * @return lastName
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

   /**
   * Get policyNr
   * @return policyNr
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getPolicyNr() {
    return policyNr;
  }

  public void setPolicyNr(String policyNr) {
    this.policyNr = policyNr;
  }



  public SwaggerParty ssn(Integer ssn) {
    this.ssn = ssn;
    return this;
  }

   /**
   * Get ssn
   * @return ssn
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public Integer getSsn() {
    return ssn;
  }

  public void setSsn(Integer ssn) {
    this.ssn = ssn;
  }

  public SwaggerParty name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public SwaggerParty ADDR(ADDR ADDR) {
    this.address = ADDR;
    return this;
  }

  /**
   * Get address
   * @return address
   **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public ADDR getAddress() {
    return address;
  }

  public void setAddress(ADDR address) {
    this.address = address;
  }




  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SwaggerParty party = (SwaggerParty) o;
    return
        Objects.equals(this.firstName, party.firstName) &&
        Objects.equals(this.lastName, party.lastName) &&
        Objects.equals(this.policyNr, party.policyNr) &&
        Objects.equals(this.ssn, party.ssn) &&
        Objects.equals(this.name, party.name) &&
        Objects.equals(this.address, party.address);

  }

  @Override
  public int hashCode() {
    return Objects.hash(firstName, lastName, policyNr, ssn, name);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Party {\n");

    sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
    sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
    sb.append("    policyNr: ").append(toIndentedString(policyNr)).append("\n");
    sb.append("    ssn: ").append(toIndentedString(ssn)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    address: ").append(toIndentedString(address)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


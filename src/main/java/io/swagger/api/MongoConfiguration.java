package io.swagger.api;

import com.mongodb.MongoClientURI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

/**
 *The mongo URI is defined in this file.
 * The URI will allow you to specify a database only, collections must
 * be specified by creating a new Repository interface
 * Change URI to cmatch your db config
 */
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
public class MongoConfiguration {

    public @Bean MongoDbFactory mongoDbFactory() throws Exception {
        String URI = "mongodb://test:password@localhost:27017/apiTest";
        System.out.println("Factory Bean called");
        //MongoClientURI("mongodb://test:password@localhost:27017/myTestDB").getURI();
        //MongoClientURI mongoSettingsStore = new MongoClientURI("mongodb://test:password@localhost:27017/myTestDB");
        MongoClientURI mongoSettingsStore = new MongoClientURI(URI);
        return new SimpleMongoDbFactory(mongoSettingsStore);
    }

}

package io.swagger.api;

import io.swagger.model.Party;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PartyRepository extends MongoRepository<Party, String> {


    //convenience functions, spring will construct the query, more advanced queries can be defined in accordance with spring documentation
    //returns a list of Party objects
    public List<Party> findByssn(Integer ssn);
    public List<Party> findBylastName(String lastName);
    public List<Party> findByphoneNr(String phoneNum);
    public List<Party> findBypolicyNr(String policyNr);
    public List<Party> findByproducerId(String producerID);
    public List<Party> findByLastNameContaining(String lastName);
    //in our test DB, some of these fields do not exist. Running a query where one of the fields don't exist will return nothing
    public List<Party> findByLastNameContainingAndPhoneNrContainingAndPolicyNrContainingAndProducerIdContaining(String lastName, String phoneNum, String policyNr, String ProducerID);
    public List<Party> findByLastNameContainingAndPhoneNrContainingAndFirstNameContaining(String lastName, String phoneNum, String firstName);

//
   // public List<Party> findByLastNameContainingAndPhoneNr(String lastName, String phoneNum);





}
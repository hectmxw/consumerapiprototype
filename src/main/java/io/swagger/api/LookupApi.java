package io.swagger.api;

import io.swagger.model.ErrorModel;
import io.swagger.model.Party;

import io.swagger.annotations.*;
import io.swagger.model.SwaggerParty;
import io.swagger.model.UserInput;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.*;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-28T05:37:13.410Z")

@Api(value = "lookup", description = "the lookup API")
public interface LookupApi {

    @ApiOperation(value = "lookupPost", notes = "Returns all information available for a specific party", response = SwaggerParty.class, responseContainer = "List", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "party response", response = SwaggerParty.class),
        @ApiResponse(code = 200, message = "unexpected error", response = SwaggerParty.class) })
    //swagger determines POST or GET from the following
    //consumes json because it consumes POST requests. can be kept
    @RequestMapping(value = "/lookup",
        produces = { "application/json" },
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<List<SwaggerParty>> lookupPost(@RequestBody UserInput userInput, @ApiParam(value = "Some SSN associated with the Party") @RequestParam(value = "SSN", required = false) String SSN,
                                                  @ApiParam(value = "Some last name associated with party") @RequestParam(value = "lastName", required = false) String lastName,
                                                  @ApiParam(value = "Some policy number associated with party") @RequestParam(value = "policyNumber", required = false) String policyNumber,
                                                  @ApiParam(value = "Some Phone Number associated with party") @RequestParam(value = "phoneNumber", required = false) String phoneNumber,
                                                  @ApiParam(value = "Some Producer ID associated with party") @RequestParam(value = "producerID", required = false) String producerID,
                                                  @ApiParam(value = "Some firstName associated with party") @RequestParam(value = "firstName", required = false) String firstName);

}
